import { Container, Navbar, Nav } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import { useState, Fragment, useContext } from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){


    const { user, setUser } = useContext(UserContext);
    console.log(user);

    const [userId, setUserId] = useState(localStorage.getItem("userId"));

    return(
        <Navbar bg="warning" expand="lg">
            <Container fluid>
                <Navbar.Brand as={Link} to="/">Pawsitively Purrfect </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mx-auto">
                        <Nav.Link as={NavLink} to ="/" >Home</Nav.Link>
                        <Nav.Link as={NavLink} to ="/products">Products</Nav.Link>  
                        <Nav.Link as={NavLink} to ="/register">Register</Nav.Link>
                    </Nav>
                    {(user.id !== null || userId !== null)?
                        <Nav.Link as={NavLink} to="/logout" >Logout</Nav.Link>
                        :
                        <Fragment>
                            <Nav className="mr-2">
                                <Nav.Link as={NavLink} to ="/login" >Login</Nav.Link>
                            </Nav>
                            
                            
                        </Fragment>
                    }
                    
                    
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}