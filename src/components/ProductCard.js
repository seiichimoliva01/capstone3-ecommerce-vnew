import { Card, Button} from 'react-bootstrap';

import { Link } from 'react-router-dom';
import "./ProductCard.css";

export default function ProductCard({productProp}) {
    const {_id, name, description, price} = productProp;

    return (
        
        <div className="cardColor">
            <Card className='card-form'>
                <Card.Body>
                    <Card.Title className="text-center">{name}</Card.Title>
                    <Card.Subtitle className="text-center">Description</Card.Subtitle>
                    <Card.Text className="text-center">{description}</Card.Text>
                    <Card.Subtitle className="text-center">Price</Card.Subtitle>
                    <Card.Text className="text-center">{price}</Card.Text>
                    <div className="button-container">
                        <Button as={Link} to={`/products/${_id}`}>Order</Button>
                    </div>
                </Card.Body>
            </Card>
        </div>
        
    );
}