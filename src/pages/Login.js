import { Form, Button } from 'react-bootstrap';
import { useState,useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import "./Login.css"

import Swal from 'sweetalert2';



export default function Login(){

    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState('');

    function authenticate(event){
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: { 'Content-type' : 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log("Check accessToken");
            console.log(data.accessToken);
            
            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token' , data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Pawsitively Purrfect!",
                    imageUrl: 'https://i.imgflip.com/2hao7l.jpg',
                    imageWidth: 350,
                    imageHeight: 350,
                    imageAlt: 'Custom image',
                })
            }
            else{
                Swal.fire({
                    title: "The authentication process was not successful.",
                    icon: "error",
                    text: "Please verify your login credentials and attempt to log in again."
                })
            }
            
        })

        setEmail('');
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
            localStorage.setItem("userId", data._id);
            localStorage.setItem("userRole", data.isAdmin);

        })
    }

    useEffect(() => {
        if(email!=='' && password!==''){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password])

    return(
        (user.id !== null)
        ?
        <Navigate to="/products" />
        :
        
        <div className="login-page">
            
            <Form className='page' onSubmit={(event) => authenticate(event)}>
            <h3 className='login'>Login</h3>
                <Form.Group  controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value = {password}
                        onChange = {event => setPassword(event.target.value)}
                        required
                    />
                </Form.Group>
                { isActive ? 
                    <Button variant="success" type="submit" id="submitBtn">
                    Login
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn"
                        disabled>
                    Provide the necessary information
                    </Button>
                
                }
                
            </Form>
            
            
           
        </div>
        
    )
}