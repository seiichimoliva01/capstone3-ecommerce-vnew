import { useState, useEffect } from 'react';

export default function Order() {
  const [user, setUser] = useState({});

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setUser(data);
    })
    .catch(error => console.log(error));
  }, []);

  return (
    <div>
      <h2>User Details</h2>
      <p>Name: {user.name}</p>
      <p>Email: {user.email}</p>
      <p>Address: {user.address}</p>
    </div>
  );
}