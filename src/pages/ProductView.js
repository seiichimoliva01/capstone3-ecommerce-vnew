import { useState, useEffect, useContext } from "react";

import {Container, Card, Button, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

//new




export default function ProductView(){

	const { user } = useContext(UserContext);

	const { productId } = useParams();
	console.log(productId)
	const navigate = useNavigate();

	const [userId] = useState(localStorage.getItem("userId"));
	
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	

	useEffect(()=>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId])

	const checkOut = (productId) =>{

		fetch(`${process.env.REACT_APP_API_URL }/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log("Product data: ")
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully Checkout",
					icon: "success",
					text: "You have made a successful purchase of this product."
				});

				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return(
		<Container>
			<Col lg={{ span: 6, offset: 3 }}>
				<Card >
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
					
						
						{
							(user.id !== null || userId !== null)
							?
								<Button variant="primary"  size="lg" onClick={() => checkOut(productId)}>Checkout</Button>
							:
								<Button as={Link} to="/login" variant="success"  size="lg">Login to Order</Button>
						}
					</Card.Body>		
				</Card>
			</Col>
			
		</Container>
	)
}

